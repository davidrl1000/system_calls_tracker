#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

char *programOptions[20];
char *trackOptionOne = "-v";
char *trackOptionTwo = "-V";
char *trackerCommand = "rastreador";
char *straceCommand = "sudo strace ";
char *defaultProgram = "/usr/bin/firefox";
char *defaultProgramOptions = "https://www.linkedin.com/in/davidrl1000/";
char *errorFortmatMessage = "\n\nInvalid input format. The input format must be like:\n\nrastreador <tracker options> <program> <program options>\n\n+++++++++ Tracker Options +++++++++\n\n -v   It will show a message each time that a system call is triggered from the created program\n -V   This option does the same of -v, but waits for an user signal from the keyboard\n\n";

typedef int bool;
#define true 1
#define false 0

bool trackerOptionOneExist = false;
bool trackerOptionTwoExist = false;
int defaultCharSize = 100;

bool analizeUserInput(const char * userInput){

    char * pch = strtok ((char*)userInput," ");

    while (pch != NULL)
    {
        if(strncmp(pch, trackerCommand,10) != 0)
        {
            return false;
        }
        else
        {
            return true;
        }            
    }

}//end analizeUserInput

const char * getUserInput(){
    
    char *userInput = malloc (sizeof (char) * defaultCharSize);
    bool validInput = false;

    do{
        printf("> ");
        fgets(userInput,defaultCharSize,stdin);
        char *input = malloc (sizeof (char) * defaultCharSize);
        strcat( input, userInput );
        validInput = analizeUserInput(input);

        if(validInput == false){
            printf ("%s",errorFortmatMessage);
        }

    }while(validInput == false);

    return userInput;
}

void getProgramToRun(const char * userInput){

    char * pch = strtok ((char*)userInput," ");
    int index = 0;
    int programOptionsIndex = 0;

    while (pch != NULL)
    {
        if(index > 0){
            if(strncmp(pch, trackOptionOne,2) == 0 && index <= 2)
            {
                trackerOptionOneExist = true;
            }
            else if(strncmp(pch, trackOptionTwo,2) == 0 && index <= 2)
            {
                trackerOptionTwoExist = true;
            } 
            else 
            {
                programOptions[programOptionsIndex] = pch;
                programOptionsIndex++;
            }
        }

        index++;
        pch = strtok (NULL, " ");
    }

    if(programOptionsIndex == 0){
        programOptions[0] = defaultProgram;
        programOptions[1] = defaultProgramOptions;
    } else {
        for(int x=0;x<=programOptionsIndex;x++){
            strtok(programOptions[x], "\n");
        }
    }

}

const char * getStraceCommand(pid_t pid){
    static char command[100] = {0};
    char newPID[30] = {0};
    char straceOptions[30] = {0};

    if(trackerOptionOneExist == true || trackerOptionTwoExist == true){
        strcat( straceOptions, "-ttTi -p " );
    }else{
        strcat( straceOptions, "-c -p " );
    }

    sprintf(newPID, "%d", (int) pid);
    strcat( command, straceCommand );
    strcat( command, straceOptions );
    strcat( command, newPID );

    return command;
}

bool captureKeyboardPressed(){
    printf ("Press any key to continue!\n");
    struct termios oldt,newt;
    int ch;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    return true;
}

void createProgram(){
    int programError;

    getProgramToRun(getUserInput());

    pid_t pid;

    if(trackerOptionTwoExist == true){
        captureKeyboardPressed();
    }

    if ((pid = fork()) < 0) {
        puts("error");
    }
    else if (pid == 0) {
        
        programError = execv(programOptions[0], programOptions);
    
    }else{
        if (programError == -1) {
            printf("An error happened running the program: %s",programOptions[0]);
        }else{
            char command[100] = {0};
            strcpy( command, getStraceCommand(pid) );

            system(command);
        }
    }
}

int main(int argc,char** argv)
{
    createProgram();
    
    return 0;
}
