# System Calls Tracker #

### The program? ###

This is a Linux system calls tracker. The tracker will run a program and track every system call triggered by the program created.

* Version 1.0
* Author: David Rojas Leitón
* TEC_ID: 2016254594
* Repository: [here](https://bitbucket.org/davidrl1000/system_calls_tracker) 

### How to run this program? ###

* Open a terminal in a Linux OS
* Go to the path where this program is store in
* run the command
```sh
./tracker
```
* The program will wait for an instruccion
* Writte an instrucction

### Instrucctions ###

Basic instruction

```sh
rastreador [tracker options]
// Example
rastreador -V
```
> If the basic instruction is entered (without a program) the software will run Firefox by default 

Main instruction
```sh
rastreador [tracker options] [program name] [program options]
// Example
rastreador -v /usr/bin/firefox
```

### Basic Tracker Options ###

**-v**: It will show a message each time that a system call is triggered from the created program

```sh
// Example
rastreador -v /usr/bin/firefox
```

**-V**: This option does the same of -v, but waits for an user signal from the keyboard

```sh
// Example
rastreador -V /usr/bin/firefox
```

> If the user uses one of the tracker options the program will print all the system calls trace, otherwise it will print a summary table at the end of the excecution 

### Test cases ###

```sh
rastreador

rastreador -V

rastreador -V /usr/lib/libreoffice/program/soffice.bin --writer

rastreador -V /usr/lib/libreoffice/program/soffice.bin --calc

rastreador /usr/bin/firefox

rastreador /usr/bin/firefox www.deepmind.com

rastreador -v -V /usr/bin/firefox www.deepmind.com

rastreador -V -v /usr/bin/firefox www.deepmind.com
```

## References ##

http://timetobleed.com/hello-world/

https://linux.die.net/man/1/strace
